using Items;
using UnityEngine;

namespace DefaultNamespace
{
    /// <summary>
    /// Consists of 2 parts
    /// </summary>
    public class EnchantingTable: MonoBehaviour,IInteractableBlock
    {
        public float ChannelTime = 5;
        private Channeler Channeler;
        public Item ItemOnTable;
        public Transform ItemTransform;
        public Transform ActiveEffect;

        public void Start()
        {
            Channeler = new Channeler(ChannelTime);
        }

        public bool TryInteract(Player player)
        {
            if (ItemOnTable == null && player.HeldItem != null)
            {
                ItemOnTable = player.HeldItem;
                player.HeldItem = null;
                ItemOnTable.transform.SetParent(ItemTransform,false);
                return true;
            }

            if (ItemOnTable != null && player.HeldItem == null && (!Channeler.IsChanneling))
            {
                player.AttachItem(ItemOnTable);
                ItemOnTable = null;
                return true;
            }

            return false;
        }

        public bool DoWork(Player player)
        {
            if (ItemOnTable == null) return false;
            if (!ItemOnTable.ItemTypeDescription.IsEnchantable) return false;

            Channeler.Channel();
            if (Channeler.IsFinished)
            {
                Channeler.Progress = 0;
                var newItem = ItemsDictionary.Instance.CreateNewItem(ItemOnTable.ItemTypeDescription.Enchanted);
                Destroy(ItemOnTable.gameObject);
                ItemOnTable = newItem;
                newItem.transform.SetParent(ItemTransform,false);
            }
            player.Channel(Channeler.Progress, ItemTransform);

            if (ActiveEffect.gameObject.activeSelf != Channeler.IsChanneling)
                ActiveEffect.gameObject.SetActive(Channeler.IsChanneling);
            return true;
        }
    }
}