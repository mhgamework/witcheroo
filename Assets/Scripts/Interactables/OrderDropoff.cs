using DefaultNamespace.Bootstrap;
using UnityEngine;

namespace DefaultNamespace
{
    public class OrderDropoff : MonoBehaviour, IInteractableBlock
    {
        public bool TryInteract(Player player)
        {
            if (player.HeldItem == null) return false;

            var item = player.HeldItem;
            var itemType = player.HeldItem.itemType;
            Destroy(item.gameObject);
            player.HeldItem = null;
            
            CompositionRoot.Active.OrderSystem.DeliverOrder(itemType);

            return true;
        }

        public bool DoWork(Player player)
        {
            return false;
        }
    }
}