using System.Collections.Generic;
using System.Linq;
using DirectX11;
using MHGameWork.TheWizards;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class GridSystem
    {
        public float CellSize = 1;
        public Dictionary<Point2, IInteractableBlock> Blocks { get; } = new Dictionary<Point2, IInteractableBlock>();


        public GridSystem(Transform root)
        {
            InitBlocks(root);
        }

        private void InitBlocks(Transform root)
        {
            foreach (var b in root.GetComponentsInChildren<IInteractableBlock>())
            {
                var mono = (MonoBehaviour) b;
                var cell = (mono.transform.position / CellSize).TakeXZ().ToFloored();
                Blocks[cell] = b;
            }
        }

        public IInteractableBlock TryGetClosestBlock(Vector3 pos, float maxDistance = 2f)
        {
            var gridCoord = (pos / CellSize).TakeXZ().ToFloored(); 
            var closestBlockPosition = Blocks.Smallest(k => Vector2.Distance(k.Key, gridCoord)).Key;

            if (Vector2.Distance(gridCoord, closestBlockPosition) <= maxDistance)
            {
                return Blocks[closestBlockPosition];
            }
            return null;
        }
    }
}