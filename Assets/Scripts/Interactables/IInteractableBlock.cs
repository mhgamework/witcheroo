namespace DefaultNamespace
{
    public interface IInteractableBlock
    {
        bool TryInteract(Player player);

        bool DoWork(Player player);
    }
}