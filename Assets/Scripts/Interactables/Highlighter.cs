﻿using UnityEngine;
using System.Collections;
using DefaultNamespace;
using System.Collections.Generic;

public class Highlighter
{
    private List<MonoBehaviour> litBlocks;
    private Color highlightColor = new Color(1,1,1,0.5f);

    public Highlighter()
    {
        litBlocks = new List<MonoBehaviour>();
    }

    public void Highlight(MonoBehaviour block)
    {
        foreach (Renderer rend in block.transform.GetComponentsInChildren<Renderer>())
        {
            if (rend.material.shader.name == "Custom/Standard")
            {
                rend.material.SetFloat("_SecondOutlineWidth", .0010f);
                rend.material.SetColor("_SecondOutlineColor", highlightColor);
                rend.material.renderQueue++;
            }
        }
        litBlocks.Add(block);
    }


    public void UnHighlight()
    {
        Debug.Log(litBlocks);
        foreach (MonoBehaviour block in litBlocks)
        {
            foreach (Renderer rend in block.transform.GetComponentsInChildren<Renderer>())
            {
                if (rend.material.shader.name == "Custom/Standard")
                {
                    rend.material.SetFloat("_SecondOutlineWidth", 0f);
                    rend.material.renderQueue--;
                }
            }
        }
        litBlocks.Clear();
    }
}
