using System;
using System.Linq;
using Assets.UnityAdditions;
using EasyButtons;
using Items;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace DefaultNamespace
{
    public class Counter : MonoBehaviour, IInteractableBlock
    {
        public ItemType InitialItem;
        
        public Transform ItemAttachPoint;

        public Item Item;
        private bool initialized = false;

        public void Init()
        {
            if (InitialItem != ItemType.None)
            {
                var item = ItemsDictionary.Instance.CreateNewItem(InitialItem);
                item.transform.SetParent(ItemAttachPoint, false);
                Item = item;
            }
        }

        public bool TryInteract(Player player)
        {
            if (player.HeldItem == null && Item != null)
            {
                player.AttachItem(Item);
                Item = null;
                return true;
            }

            if (player.HeldItem != null && Item == null)
            {
                AttachItem(player.HeldItem);
                Item = player.HeldItem;
                player.HeldItem = null;
                return true;
            }

            return false;
        }

        public bool DoWork(Player player)
        {
            return false;
        }

        private void AttachItem(Item h)
        {
            h.transform.SetParent(ItemAttachPoint, false);
        }

        public void OnValidate()
        {
            #if UNITY_EDITOR
            EditorApplication.delayCall += () =>
            {
                if (Application.isPlaying) return;
                if (!ItemAttachPoint) return;
                foreach (var c in ItemAttachPoint.GetChildren<Transform>()
                    .Where(f => f.gameObject.hideFlags == HideFlags.HideAndDontSave))
                {
                    DestroyImmediate(c.gameObject);
                }

                if (InitialItem != ItemType.None)
                {
                    var item = ItemsDictionary.InstantiateItemRenderer(InitialItem);
                    item.gameObject.hideFlags = HideFlags.HideAndDontSave;
                    item.SetParent(ItemAttachPoint, false);
                }
            };
            #endif
        }

    
    }
}