﻿using UnityEngine;

namespace DefaultNamespace
{
    public class Well: MonoBehaviour, IInteractableBlock
    {
        public float ChannelTime = 3f;
        private Channeler Channeler;
        public bool HasBucket;
        public Transform WellBucketPosition;
        public Transform WellBucketUp;
        public Transform WellBucketDown;

        public Item fullBucket;

        public void Start()
        {
            Channeler = new Channeler(ChannelTime);
            WellBucketUp.gameObject.SetActive(false);
            WellBucketDown.gameObject.SetActive(false);
        }
        
        public bool TryInteract(Player player)
        {
            if (player.HeldItem == null)
            {
                if (HasBucket && Channeler.IsFinished)
                {
                    player.AttachItem(Instantiate(fullBucket));
                    HasBucket = false;
                    Channeler.Progress = 0;
                    return true;
                }
            }
            else
            {
                if ( player.HeldItem.itemType == ItemType.BucketEmpty)
                {
                    Destroy(player.HeldItem.gameObject);
                    player.HeldItem = null;
                    HasBucket = true;
                    return true;
                }
            }
            return false;
        }

        public bool DoWork(Player player)
        {
            if (!HasBucket) return false;
            if (Channeler.IsFinished)
            {
                player.Channel(0, null);
                return false;
            }

            Channeler.Channel();
            player.Channel(Channeler.Progress, transform);
            return true;
        }

        public void Update()
        {
            if (!HasBucket)
            {
                WellBucketUp.gameObject.SetActive(false);
                WellBucketDown.gameObject.SetActive(false);
                return;
            }

            var Direction = (Vector3.down * 2) * EasingFunction.EaseInOutSine(0f, 1f, Channeler.Progress * 2);
            if (Channeler.Progress > 0.5)
            {
                WellBucketUp.gameObject.SetActive(true);
                WellBucketDown.gameObject.SetActive(false);
            }
            else
            {
                WellBucketUp.gameObject.SetActive(false);
                WellBucketDown.gameObject.SetActive(true);
            }
            WellBucketPosition.transform.localPosition = Direction;
        }
    }
}