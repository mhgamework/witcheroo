using System;
using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace
{
    public class HiddenTile: MonoBehaviour
    {
        public void OnEnable()
        {
            GetComponentInChildren<MeshRenderer>().enabled = false;
        }
    }
}