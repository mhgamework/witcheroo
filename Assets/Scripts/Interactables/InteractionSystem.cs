namespace DefaultNamespace
{
    public class InteractionSystem
    {
        private GridSystem gridSystem;

        public InteractionSystem(GridSystem gridSystem)
        {
            this.gridSystem = gridSystem;
        }

        /// <summary>
        /// TODO: take into account player look direction, and maybe raycast?
        /// </summary>
        /// <param name="p"></param>
        public IInteractableBlock TryGetTargetedInteractable(Player p)
        {
            // Closest
            return gridSystem.TryGetClosestBlock(p.transform.position);
        }
    }
}