﻿using Cauldron;
using DefaultNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Interactables.Cauldron
{
    public class BrewingPotion: CauldronLiquid
    {
        private float Progress;
        private float Duration = 18;
        private List<ItemType> Ingredients = new List<ItemType>();
        private float ColorProgress;
        private float ColorDuration = 2;
        private List<ItemType> PossibleIngredients = new List<ItemType> {ItemType.None, ItemType.MushroomEnchanted, ItemType.FrogEnchanted, ItemType.FishEnchanted};

        public override CauldronLiquid AddIngredient(Item item)
        {
            if (GetCurrentPossibleIngredient() == item.itemType)
            {
                Ingredients.Add(item.itemType);
                ColorProgress = 0;
                PossibleIngredients.Remove(item.itemType);
                return this;
            }
            else
            {
                return LiquidFactory.Instance.CreatePoopSoup();
            }
        }

        public void AddInitialIngredient(Item item)
        {
                var mr = (MeshRenderer)gameObject.GetComponentInChildren(typeof(MeshRenderer));
                mr.material.color = Color.black;
                Ingredients.Add(item.itemType);
                PossibleIngredients.Remove(item.itemType);
        }

        public ItemType GetCurrentPossibleIngredient()
        {
            return PossibleIngredients[Mathf.FloorToInt(ColorProgress % PossibleIngredients.Count)];
        }

        public override bool IsPotion()
        {
            return false;
        }

        public override CauldronLiquid Cook()
        {
            Progress += Time.deltaTime / Duration;
            if (Progress > 1)
            {
                if (Ingredients.Contains(ItemType.MushroomEnchanted) && Ingredients.Contains(ItemType.FrogEnchanted) && Ingredients.Contains(ItemType.FishEnchanted))
                {
                    return LiquidFactory.Instance.CreateFinishedPotion(ItemType.MushroomFrogFishPotion);
                }
                else if (Ingredients.Contains(ItemType.MushroomEnchanted) && Ingredients.Contains(ItemType.FrogEnchanted))
                {
                    return LiquidFactory.Instance.CreateFinishedPotion(ItemType.MushroomFrogPotion);
                }
                else if (Ingredients.Contains(ItemType.MushroomEnchanted) && Ingredients.Contains(ItemType.FishEnchanted))
                {
                    return LiquidFactory.Instance.CreateFinishedPotion(ItemType.MushroomFishPotion);
                }
                else if (Ingredients.Contains(ItemType.FrogEnchanted) && Ingredients.Contains(ItemType.FishEnchanted))
                {
                    return LiquidFactory.Instance.CreateFinishedPotion(ItemType.FrogFishPotion);
                }
                else if (Ingredients.Contains(ItemType.MushroomEnchanted))
                {
                    return LiquidFactory.Instance.CreateFinishedPotion(ItemType.MushroomPotion);
                }
                else if (Ingredients.Contains(ItemType.FrogEnchanted))
                {
                    return LiquidFactory.Instance.CreateFinishedPotion(ItemType.FrogPotion);
                }
                else if (Ingredients.Contains(ItemType.FishEnchanted))
                {
                    return LiquidFactory.Instance.CreateFinishedPotion(ItemType.FishPotion);
                }
                else
                {
                    // not good
                    return null;
                }  
            }
            var previousIngredient = GetCurrentPossibleIngredient();
            ColorProgress += Time.deltaTime / ColorDuration;
            if (!GetCurrentPossibleIngredient().Equals(previousIngredient))
            {
                var mr = (MeshRenderer)gameObject.GetComponentInChildren(typeof(MeshRenderer));
                switch (GetCurrentPossibleIngredient())
                {
                    case ItemType.None:
                        mr.material.color = Color.black;
                        break;
                    case ItemType.MushroomEnchanted:
                        mr.material.color = Color.red;
                        break;
                    case ItemType.FishEnchanted:
                        mr.material.color = Color.blue;
                        break;
                    case ItemType.FrogEnchanted:
                        mr.material.color = Color.green;
                        break;
                    default:
                        break;
                }
            }
            return this;
        }

        public override bool CanAcceptIngredient()
        {
            return true;
        }

        public override ItemType GetPotion()
        {
            return ItemType.None;
        }

        public override float GetProgress()
        {
            return Progress;
        }
    }
}
