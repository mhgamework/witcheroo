﻿using DefaultNamespace;
using System.Collections.Generic;
using UnityEngine;

namespace Cauldron
{
    public enum LiquidType { Water, PoopSoup, BrewingPotion, FinishedPotion}
    
    public abstract class CauldronLiquid:MonoBehaviour
    {
        public abstract CauldronLiquid AddIngredient(Item item);

        public abstract bool IsPotion();

        public abstract ItemType GetPotion();

        public abstract CauldronLiquid Cook();

        public abstract bool CanAcceptIngredient();

        public virtual float GetProgress()
        {
            return -1;
        }
    }
}