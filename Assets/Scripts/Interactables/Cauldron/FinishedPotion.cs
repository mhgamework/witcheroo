﻿using Cauldron;
using DefaultNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Interactables.Cauldron
{
    public class FinishedPotion:CauldronLiquid
    {
        private float Duration = 6;
        private float Progress;
        public ItemType Potion;

        public override CauldronLiquid AddIngredient(Item item)
        {
            return this;
        }

        public override bool IsPotion()
        {
            return true;
        }

        public override CauldronLiquid Cook()
        {
            Progress += Time.deltaTime / Duration;
            if (Progress > 1)
            {
                return LiquidFactory.Instance.CreatePoopSoup();
            }
            return this;
    }

        public override bool CanAcceptIngredient()
        {
            return false;
        }

        public override ItemType GetPotion()
        {
            return Potion;
        }

        public override float GetProgress()
        {
            return Progress;
        }
    }
}
