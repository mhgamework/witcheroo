﻿using Cauldron;
using DefaultNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Interactables.Cauldron
{
    public class CauldronWater : CauldronLiquid
    {
        public override CauldronLiquid AddIngredient(Item item)
        {
            if (ItemType.PotionFailed.Equals(item.itemType))
            {
                return LiquidFactory.Instance.CreatePoopSoup();
            }
            else
            {
                return LiquidFactory.Instance.CreateBrewingPotion(item);
            }
        }

        public override bool IsPotion()
        {
            return false;
        }

        public override CauldronLiquid Cook()
        {
            return this;
        }

        public override bool CanAcceptIngredient()
        {
            return true;
        }

        public override ItemType GetPotion()
        {
            return ItemType.None;
        }
    }
}
