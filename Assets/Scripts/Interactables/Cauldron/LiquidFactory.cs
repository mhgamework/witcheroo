﻿using System;
using System.Collections.Generic;
using Assets.Modules.MHGameWork.Reusable.UnityAdditions;
using Assets.Scripts.Interactables.Cauldron;
using DefaultNamespace;
using UnityEngine;

namespace Cauldron
{
    [ExecuteInEditMode]
    public class LiquidFactory : ManualSingleton<LiquidFactory>
    {
        public CauldronWater CauldronWater;
        public BrewingPotion BrewingPotion;
        public PoopSoup PoopSoup;
        public FinishedPotion FinishedPotion;

        public BrewingPotion CreateBrewingPotion(Item initialIngredient)
        {
            var brewingPotion = Instantiate(BrewingPotion);
            brewingPotion.AddInitialIngredient(initialIngredient);
            return brewingPotion;
        }

        public CauldronWater CreateCauldronWater()
        {
            return Instantiate(CauldronWater);
        }

        public PoopSoup CreatePoopSoup()
        {
            return Instantiate(PoopSoup);
        }

        public FinishedPotion CreateFinishedPotion(ItemType potion)
        {
            var finishedPotion = Instantiate(FinishedPotion);
            finishedPotion.Potion = potion;
            return finishedPotion;
        }

    }
}