﻿using Cauldron;
using DefaultNamespace;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Interactables.Cauldron
{
    public class PoopSoup:CauldronLiquid
    {
        public override CauldronLiquid AddIngredient(Item item)
        {
            return this;
        }

        public override bool IsPotion()
        {
            return true;
        }

        public override CauldronLiquid Cook()
        {
            return this;
        }

        public override bool CanAcceptIngredient()
        {
            return false;
        }

        public override ItemType GetPotion()
        {
            return ItemType.PotionFailed;
        }
    }
}
