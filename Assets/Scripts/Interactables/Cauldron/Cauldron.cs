﻿using System.Collections.Generic;
using DefaultNamespace;
using Items;
using UnityEngine;
using UnityEngine.UI;

namespace Cauldron
{
    public class Cauldron : MonoBehaviour, IInteractableBlock
    {
        public Transform LiquidAttachPoint;
        private CauldronLiquid currentLiquid;
        public Image ProgressBar;
        public Image Background;

        public void Start()
        {
            if (ProgressBar != null)
            {
                toggleProgressBar(false);
            }
        }

        public bool TryInteract(Player player)
        {
            if (player.HeldItem != null)
            {
                // water in de cauldron
                if (player.HeldItem.itemType == ItemType.BucketFull && currentLiquid == null)
                {
                    Destroy(player.HeldItem.gameObject);
                    player.AttachItem(ItemsDictionary.Instance.CreateNewItem(ItemType.BucketEmpty));
                    SwitchLiquid(LiquidFactory.Instance.CreateCauldronWater());
                }
                // ingrediënt in een niet lege cauldron
                else if (player.HeldItem.ItemTypeDescription.IsCauldronIngredient && currentLiquid != null && currentLiquid.CanAcceptIngredient())
                {
                    var nextLiquid = currentLiquid.AddIngredient(player.HeldItem);
                    SwitchLiquid(nextLiquid);
                    Destroy(player.HeldItem.gameObject);
                    player.AttachItem(ItemsDictionary.Instance.CreateNewItem(ItemType.VialEmpty));
                } 
                // liquid eruit halen
                else if (player.HeldItem.itemType == ItemType.VialEmpty && currentLiquid != null && currentLiquid.IsPotion())
                {
                    Destroy(currentLiquid.gameObject);
                    Destroy(player.HeldItem.gameObject);
                    player.AttachItem(ItemsDictionary.Instance.CreateNewItem(currentLiquid.GetPotion()));
                    currentLiquid = null;
                }
            }
            return false;
        }

        private void SwitchLiquid(CauldronLiquid liquid)
        {
            if (!liquid.Equals(currentLiquid))
            {
                if (currentLiquid != null)
                {
                    Destroy(currentLiquid.gameObject);
                }
                currentLiquid = liquid;
                if (currentLiquid == null)
                {
                    toggleProgressBar(false);
                }
                liquid.transform.SetParent(LiquidAttachPoint, false);
            }
        }

        public bool DoWork(Player player)
        {
            return false;
        }

        public void Update()
        {
            if (currentLiquid != null)
            {
                ShowProgress(currentLiquid.GetProgress());
                SwitchLiquid(currentLiquid.Cook());
            }
            else
            {
                toggleProgressBar(false);
            }
        }

        public void ShowProgress(float progress)
        {
            if (progress >= 0)
            {
                toggleProgressBar(true);
            }
            else
            {
                toggleProgressBar(false);
            }
            ProgressBar.fillAmount = progress;
        }

        private void toggleProgressBar(bool enabled)
        {
            ProgressBar.enabled = enabled;
            Background.enabled = enabled;
        }
    }
}