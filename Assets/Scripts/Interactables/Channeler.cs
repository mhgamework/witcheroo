using UnityEngine;

namespace DefaultNamespace
{
    public class Channeler
    {
        public float ChannelTime = 0;
        public float Progress = 0;
        public bool IsChanneling => Progress > 0;
        public bool IsFinished => Progress > 1;

        public Channeler(float ChannelTime)
        {
            this.ChannelTime = ChannelTime;
        }

        public void Channel()
        {
            Progress += Time.deltaTime / ChannelTime;
        }
    }
}