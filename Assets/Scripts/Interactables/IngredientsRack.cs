using System.Collections.Generic;
using Assets.UnityAdditions;
using Items;
using UnityEditor;
using UnityEngine;

namespace DefaultNamespace
{
    [ExecuteInEditMode]
    public class IngredientsRack : MonoBehaviour, IInteractableBlock
    {
        public ItemType ItemType;
        public List<Transform> ItemSlots;
        private ItemType RenderedItemType;

        public bool TryInteract(Player player)
        {
            if (player.HeldItem != null) return false;

            var newItem = ItemsDictionary.Instance.CreateNewItem(ItemType);
            player.AttachItem(newItem);
            return true;
        }

        public bool DoWork(Player player)
        {
            return false;
        }

        public void Update()
        {
            if (RenderedItemType != ItemType)
            {
                foreach (var f in ItemSlots)
                {
                    foreach (var i in f.transform.GetChildren<Transform>())
                        if (i.gameObject.hideFlags == HideFlags.HideAndDontSave)
                            DestroyImmediate(i.gameObject);
                }

                if (ItemType != ItemType.None)
                {
                    foreach (var f in ItemSlots)
                    {
                        var item = ItemsDictionary.InstantiateItemRenderer(ItemType);
                        item.gameObject.hideFlags = HideFlags.HideAndDontSave; // Only editor!! awesome
                        item.SetParent(f);
                        item.transform.localPosition = new Vector3();
                        item.transform.localRotation = Quaternion.identity;
                        item.transform.localScale = Vector3.one;
                    }
                }

                RenderedItemType = ItemType;
            }
        }
    }
}