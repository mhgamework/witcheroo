using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace DefaultNamespace
{
    public enum ItemType
    {
        None,
        BucketEmpty,
        BucketFull,
        VialEmpty,
        PotionFailed,
        
        Mushroom,
        MushroomEnchanted,
        MushroomPotion,
        
        Fish,
        FishEnchanted,
        FishPotion,
        
        Frog,
        FrogEnchanted,
        FrogPotion,
        
        MushroomFishPotion,
        MushroomFrogPotion,
        FrogFishPotion,
        MushroomFrogFishPotion,
    }

    [Serializable]
    public class ItemTypeDescription
    {
        public ItemType ItemType;
        public ItemType Enchanted;
        public bool IsEnchantable => Enchanted != ItemType.None;
        public bool IsCauldronIngredient = false;
    }
    
    public class Item : MonoBehaviour
    {
        public ItemType itemType => ItemTypeDescription.ItemType;
        public ItemTypeDescription ItemTypeDescription;
    }
}