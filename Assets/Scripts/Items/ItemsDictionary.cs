using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Modules.MHGameWork.Reusable.UnityAdditions;
using Assets.Reusable;
using DefaultNamespace;
using TMPro;
using UnityEngine;

namespace Items
{
    public class ItemsDictionary : ManualSingleton<ItemsDictionary>
    {
        private List<Item> prefabs = new List<Item>();

        private Dictionary<ItemType, Item> dict = new Dictionary<ItemType, Item>();
        private Dictionary<ItemType, ItemTypeDescription> descs = new Dictionary<ItemType, ItemTypeDescription>();

        public void OnEnable()
        {
            registerItems();
        }

        public Item TryGetPrefab(ItemType type)
        {
            return dict.GetOrDefault(type, null);
        }

        public Item CreateNewItem(ItemType type)
        {
            var prefab = dict[type];
            return Instantiate(prefab);
        }

        public ItemTypeDescription GetTypeDescription(ItemType type)
        {
            return descs[type];
        }

        /// <summary>
        /// For use in editor!
        /// </summary>
        public static Transform InstantiateItemRenderer(ItemType itemType)
        {
            //TODO: this assumes the enum names match the prefabs.
            var item = Resources.Load<Item>($"Items/{itemType}");
            if (item == null)
            {
                Debug.LogError($"Couldnt find prefab for itemtype {itemType}");
            }
            item = Instantiate(item);
            var trans = item.transform;
            DestroyImmediate(item); // Remove, we will only use renderer
            trans.position = new Vector3();
            trans.rotation = Quaternion.identity;
            trans.localScale = Vector3.one;
            return trans;
        }
   
        private void registerItems()
        {
            prefabs = Resources.LoadAll<Item>("Items").ToList();
            dict.Clear();
            descs.Clear();
            foreach (var p in prefabs)
            {
                dict.Add(p.ItemTypeDescription.ItemType,p);
                descs.Add(p.ItemTypeDescription.ItemType,p.ItemTypeDescription);
            }
        }
    }
}