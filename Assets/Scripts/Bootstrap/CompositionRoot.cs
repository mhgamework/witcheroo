using System.Linq;
using DefaultNamespace.Menus;
using DefaultNamespace.Multiplaying;
using MHGameWork.TheWizards.DualContouring;
using Recipes;
using UnityEngine;

namespace DefaultNamespace.Bootstrap
{
    /// <summary>
    /// Placeholder class to hold all the di
    /// </summary>
    public class CompositionRoot
    {
        public static CompositionRoot Active { get; set; }
        public GridSystem GridSystem { get; private set; }
        public InteractionSystem InteractionSystem { get; private set; }
        public GameSimulator GameSimulator { get; private set; }
        public ConfigLoader ConfigLoader { get; private set; }
        public OrderSystem OrderSystem { get; private set; }
        public ScoreDialogScript ScoreDialog { get; private set; }
        public LevelStats LevelStats { get; private set; }
        public RecipeDictionary RecipeDictionary { get; private set; }
        public MultiplayerRoom Room { get; private set; }
        
        private CompositionRoot()
        {
        }

        public static CompositionRoot CreateDefault(Transform root, int networkId,LevelDescription levelDescription)
        {
            Services.EnsureAppInitialized();
            
            var ret = new CompositionRoot();
            ret.Room = new MultiplayerRoom(root, networkId, NetworkedObjectRegistry.Instance);

            Object.Instantiate(CoreSystemsScript.Instance.GameHUD, root);
            
            ret.ConfigLoader = new ConfigLoader();
            ret.LevelStats = new LevelStats(){Description =  levelDescription};
            ret.GridSystem = new GridSystem(root);
            ret.InteractionSystem = new InteractionSystem(ret.GridSystem);
            ret.RecipeDictionary = new RecipeDictionary();
            ret.OrderSystem = new OrderSystem(ret.LevelStats,ret.RecipeDictionary);
            ret.GameSimulator = new GameSimulator(ret.InteractionSystem,ret.ConfigLoader,ret.Room,ret.OrderSystem,ret.LevelStats);

            ret.ScoreDialog = Object.Instantiate(CoreSystemsScript.Instance.ScoreDialog);
            ret.ScoreDialog.Init();

            

            ret.Room.GetAll<OrderPanelScript>().FirstOrDefault()?.Init(ret.OrderSystem);
            ret.Room.GetAll<ScoreViewScript>().FirstOrDefault()?.Init(ret.LevelStats);
            
            ret.Room.GetAll<Counter>().ForEach(c => c.Init());
            ret.Room.GetAll<LevelTimerScript>().ForEach(c => c.Init(ret.LevelStats));
            
            return ret;
        }

        
    }
}