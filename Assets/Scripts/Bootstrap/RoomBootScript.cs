using DefaultNamespace.Menus;
using DefaultNamespace.Multiplaying;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DefaultNamespace.Bootstrap
{
    /// <summary>
    /// Root script that identifies a component as a container for a room.
    /// </summary>
    public class RoomBootScript : MonoBehaviour
    {
        public bool RunMultiplayer;
        public LevelDescription LevelDescription;
        
        // private bool started = false;
        private bool isClient;

        public void Update()
        {
            // if (!started)
            {
                enabled = false;
                // started = true;
                
                // individual
                if (!RunMultiplayer)
                {
                    var root = CompositionRoot.CreateDefault(transform,0,LevelDescription);
                    
                    var mp = gameObject.AddComponent<MultiplayerServerTestScript>();
                    mp.InitAndStartServer(root,true);
                }
                else
                {
                    // Assign player network ids
                    var i = 0;
                    foreach (var c in transform.GetComponentsInChildren<NetworkedObjectScript>())
                    {
                        c.GetComponent<NetworkedObjectScript>().NetworkId = i++;
                    }

                    i = 0;
                    foreach (var c in transform.GetComponentsInChildren<Player>())
                    {
                        c.GetComponent<NetworkedObjectScript>().Owner = i++;

                    }

                    
                    var serverTransform = transform;
                    var clientTransform = Instantiate(this).transform;
                    clientTransform.transform.localPosition += new Vector3(20,0,0);
                    Physics.SyncTransforms(); // Sync transforms after the copy!

                    var serverRoot = CompositionRoot.CreateDefault(serverTransform,0,LevelDescription);
                    var clientRoot = CompositionRoot.CreateDefault(clientTransform,1,LevelDescription);
                    
                    var mp = serverTransform.gameObject.AddComponent<MultiplayerServerTestScript>();
                    mp.InitAndStartServer(serverRoot,false);

                    var cl = clientTransform.gameObject.AddComponent<MultiplayerClientTestScript>();
                    cl.InitAndConnect(clientRoot);
                }
            }
        }
    }
}