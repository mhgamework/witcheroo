using System.Linq;
using DefaultNamespace.Menus;
using DefaultNamespace.Multiplaying;
using Recipes;
using UnityEngine;

namespace DefaultNamespace.Bootstrap
{
    /// <summary>
    /// Responsible for root gameplay simulation logic
    /// </summary>
    public class GameSimulator
    {
        private InteractionSystem interactionSystem;
        private ConfigLoader config;

        private MultiplayerRoom room;
        private readonly OrderSystem orderSystem;
        private Highlighter highlighter;

        private LevelStats levelStats;


        public GameSimulator(InteractionSystem interactionSystem,
            ConfigLoader config,
            MultiplayerRoom room,
            OrderSystem orderSystem,LevelStats levelStats)
        {
            this.interactionSystem = interactionSystem;
            this.config = config;
            this.room = room;
            this.orderSystem = orderSystem;
            this.levelStats = levelStats;
            this.highlighter = new Highlighter();
        }

        public void StartLevel(LevelDescription levelDescription)
        {
            levelStats.Description = levelDescription;
            levelStats.IsLevelPlaying = true;
            levelStats.StartTime = Time.timeSinceLevelLoad;
        }

        public void SimulateGameLogic()
        {
            if (!levelStats.IsLevelPlaying) return;
            orderSystem.UpdateOrders();
            if (levelStats.StartTime + levelStats.Description.LevelDurationSeconds+1 < Time.timeSinceLevelLoad
                || Input.GetKeyDown(KeyCode.K))
            {
                onGameTimeExpired();
            }
        }

        private void onGameTimeExpired()
        {
            levelStats.IsLevelPlaying = false;
            CompositionRoot.Active.ScoreDialog.Show( levelStats, () => GameStateService.RestartCurrentLevel(), () => GameStateService.SwitchToMenu());
        }

        public void ApplyPlayerInputs()
        {
            if (!levelStats.IsLevelPlaying) return;

            var players = room.GetAllOwnedObjects<Player>();

            highlighter.UnHighlight();
            foreach (var p in players)
            {
                var interact = interactionSystem.TryGetTargetedInteractable(p);
                if (interact == null)
                {
                    continue;
                }

                highlighter.Highlight(interact as MonoBehaviour);

                if (p.keySet.GetPlaceTake())
                {
                    interact.TryInteract(p);
                }

                if (p.keySet.GetChannel())
                {
                    interact.DoWork(p);
                }
            }
        }

        public void SimulatePlayerMovementFixedTimestep()
        {
            var players = room.GetAllOwnedObjects<Player>();

            foreach (var p in players)
            {
                p.SimulateMovementInput();
            }
        }
    }
}