using System;
using Object = UnityEngine.Object;

namespace DefaultNamespace.Bootstrap
{
    /// <summary>
    /// Responsible for lifetime of app-level services. These services exists during the entire lifetime of the application.
    /// Services include menu state, player input mapping, game saving.
    /// </summary>
    public class AppCompositionRoot
    {
        public static AppCompositionRoot Create()
        {
            CoreSystemsScript.EnsureLoaded();

            Object.Instantiate(CoreSystemsScript.Instance.RenderToTextureScript);
            Object.Instantiate(CoreSystemsScript.Instance.EventSystem);
            Object.Instantiate(CoreSystemsScript.Instance.NetworkedObjectRegistry);
            Object.Instantiate(CoreSystemsScript.Instance.PrefabsDatabase);
            Object.Instantiate(CoreSystemsScript.Instance.ItemsDictionary);
            Object.Instantiate(CoreSystemsScript.Instance.LiquidFactory);
            
            return new AppCompositionRoot();
        }
    }
}