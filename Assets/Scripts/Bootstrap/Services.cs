using System;
using UnityEngine;

namespace DefaultNamespace.Bootstrap
{
    /// <summary>
    /// Static class holding the current services state
    /// </summary>
    public static class Services
    {
        public static AppCompositionRoot App {get; private set; }
        
        public static void EnsureAppInitialized()
        {
            if (App != null) return;
            App = AppCompositionRoot.Create();
        }

        public static void WithContext(CompositionRoot compositionRoot, Action action)
        {
            try
            {
                CompositionRoot.Active = compositionRoot;
                action();
            }
            finally
            {
                CompositionRoot.Active = null;
            }
        }
    }
}