using Assets.Modules.MHGameWork.Reusable.UnityAdditions;
using Cauldron;
using DefaultNamespace.Menus;
using DefaultNamespace.Multiplaying;
using Items;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DefaultNamespace.Bootstrap
{
    public class CoreSystemsScript : ManualSingleton<CoreSystemsScript>
    {
        public Transform GameHUD;
        public ItemsDictionary ItemsDictionary;
        public LiquidFactory LiquidFactory;
        public NetworkedObjectRegistry NetworkedObjectRegistry;
        public PrefabsDatabase PrefabsDatabase;
        public RenderToTextureScript RenderToTextureScript;
        public ScoreDialogScript ScoreDialog;
        public EventSystem EventSystem;

        public static void EnsureLoaded()
        {
            if (!CoreSystemsScript.HasInstance)
            {
                var systems = Instantiate(Resources.Load<CoreSystemsScript>("CoreSystems"));
                Object.DontDestroyOnLoad(systems);
            }
        }
    }
}