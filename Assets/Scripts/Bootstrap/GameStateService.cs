using UnityEngine.SceneManagement;

namespace DefaultNamespace.Bootstrap
{
    /// <summary>
    /// Responsible for switching between root-level application state
    /// Menus, stats screen, play mode, options etc
    /// </summary>
    public class GameStateService
    {
        public static void SwitchToMenu()
        {
            SceneManager.LoadScene("Menu");
        }

        public static void SwitchToLevel( string sceneName)
        {
            SceneManager.LoadScene(sceneName);
        }

        public static void RestartCurrentLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        
    }
}