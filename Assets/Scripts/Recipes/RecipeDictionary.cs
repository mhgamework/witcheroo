using System;
using System.Collections.Generic;
using DefaultNamespace;

namespace Recipes
{
    /// <summary>
    /// Currently hardcoded, used to lookup recipes for potions
    /// </summary>
    public class RecipeDictionary
    {
        private Dictionary<ItemType, Recipe> recipes = new Dictionary<ItemType, Recipe>();
        public RecipeDictionary()
        {
            addRecipeEnchants(ItemType.MushroomPotion,ItemType.MushroomEnchanted);        
            addRecipeEnchants(ItemType.FishPotion,ItemType.FishEnchanted);        
            addRecipeEnchants(ItemType.FrogPotion,ItemType.FrogEnchanted);
            
            addRecipeEnchants(ItemType.FrogFishPotion, ItemType.FrogEnchanted,ItemType.FishEnchanted);
            addRecipeEnchants(ItemType.MushroomFishPotion, ItemType.MushroomEnchanted,ItemType.FishEnchanted);
            addRecipeEnchants(ItemType.MushroomFrogPotion, ItemType.MushroomEnchanted,ItemType.FrogEnchanted);
            
            addRecipeEnchants(ItemType.MushroomFrogFishPotion, ItemType.MushroomEnchanted,ItemType.FrogEnchanted,ItemType.FishEnchanted);

        }

        private void addRecipeEnchants(ItemType result, params ItemType[] enchantedIngredients)
        {
            var recipe = new Recipe();
            foreach (var i in enchantedIngredients)
            {
                recipe.Ingredients.Add(new RecipeIngredient() {ItemType = i, OperationText = "Ench"});
            }
            recipe.DeliveredType = result;
            recipes.Add(result,recipe);
        }

        public Recipe GetRecipeForPotion(ItemType recipeSpecResultPotion)
        {
            return recipes[recipeSpecResultPotion];
        }
    }
}