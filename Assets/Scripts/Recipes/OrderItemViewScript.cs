using Items;
using UnityEngine;
using UnityEngine.UI;

namespace Recipes
{
    public class OrderItemViewScript : MonoBehaviour
    {
        public RawImage OutputImage;
        public Image Progress;
        public Text txtOutput;
        private Order order;

        public void Show(Order order)
        {
            this.order = order;
            gameObject.SetActive(true);
            var views = GetComponentsInChildren<OrderIngredientViewScript>(true);
            var ingredients = order.Recipe.Ingredients;

            for (int i = 0; i < views.Length; i++)
            {
                if (ingredients.Count <= i)
                    views[i].Hide();
                else
                    views[i].Show(ingredients[i]);
            }

            txtOutput.text = order.Recipe.DeliveredType.ToString();
            OutputImage.texture = RenderToTextureScript.Instance()
                .PrefabToTexture(ItemsDictionary.Instance.TryGetPrefab(order.Recipe.DeliveredType).gameObject);
        }

        public void Update()
        {
            if (order == null) return;
            Progress.fillAmount = order.PercentageExpired;
        }
    }
}