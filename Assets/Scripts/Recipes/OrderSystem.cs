using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Modules.MHGameWork.Reusable.UnityAdditions;
using DefaultNamespace;
using DefaultNamespace.Menus;
using UnityEngine;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Recipes
{
    public class OrderSystem
    {
        public float NextRecipe = -1;
        public List<Order> activeOrders = new List<Order>();
        private LevelStats levelStats;
        private readonly RecipeDictionary recipeDictionary;
        public event Action OnChange;

        public OrderSystem(LevelStats levelStats, RecipeDictionary recipeDictionary)
        {
            this.levelStats = levelStats;
            this.recipeDictionary = recipeDictionary;
        }

        public void UpdateOrders()
        {
            foreach (var r in activeOrders.ToList())
            {
                if (r.ExpirationTime < Time.timeSinceLevelLoad)
                {
                    activeOrders.Remove(r);
                    OnChange?.Invoke();
                    levelStats.Score -= 20;
                    levelStats.NumFailedPotions++;
                }
            }

            if (NextRecipe < 0) NextRecipe = this.levelStats.Description.FirstRecipeTime;
            if (NextRecipe < Time.timeSinceLevelLoad && levelStats.Description.Recipes.Count > 0)
            {
                var desc = levelStats.Description;
                var recipeSpec = pickRandomRecipeSpec(desc);

                var recipe = recipeDictionary.GetRecipeForPotion(recipeSpec.ResultPotion);

                NextRecipe = Time.timeSinceLevelLoad + recipeSpec.GetRandomNextRecipeInterval();

                Order order;
                order = new Order() {Recipe = recipe};
                order.OrderTime = Time.timeSinceLevelLoad;
                order.ExpirationTime = Time.timeSinceLevelLoad + recipeSpec.GetRandomOrderExpirationTime();

                addOrder(order);
            }
        }


        private static LevelRecipeSpec pickRandomRecipeSpec(LevelDescription desc)
        {
            var totalChance = desc.Recipes.Sum(f => f.ChanceForThisPotion);
            var roll = Random.value * totalChance;
            var i = -1;
            while (roll > 0 && i < desc.Recipes.Count)
            {
                i++;
                roll -= desc.Recipes[i].ChanceForThisPotion;
            }

            var recipeSpec = desc.Recipes[i];
            return recipeSpec;
        }

        // private float GetRecipeDuration()
        // {
        //     return 40 + Random.Range(0, 60);
        // }
        //
        // private float GetRecipeInterval()
        // {
        //     return 15 + Random.Range(0, 5);
        // }

        private void addOrder(Order order)
        {
            activeOrders.Add(order);
            OnChange?.Invoke();
        }

        public void DeliverOrder(ItemType type)
        {
            var order = activeOrders.FirstOrDefault(o => o.Recipe.DeliveredType == type);
            if (order != null)
            {
                levelStats.Score += order.Points;
                levelStats.NumDeliveredPotions++;
                activeOrders.Remove(order);
                OnChange?.Invoke();
            }
            else
            {
                levelStats.Score -= 5;
            }
        }
    }
}