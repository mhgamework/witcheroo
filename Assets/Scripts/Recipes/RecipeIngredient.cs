using DefaultNamespace;

namespace Recipes
{
    public class RecipeIngredient
    {
        public ItemType ItemType;
        public string IconText;
        public string OperationText;
    }
}