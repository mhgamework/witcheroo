using DefaultNamespace.Menus;
using UnityEngine;
using UnityEngine.UI;

namespace Recipes
{
    public class ScoreViewScript : MonoBehaviour
    {
        private LevelStats levelStats;

        public void Init(LevelStats levelStats)
        {
            this.levelStats = levelStats;
        }

        public void Update()
        {
            if (levelStats == null) return;
            
            GetComponent<Text>().text = levelStats.Score.ToString();
        }
    }
}