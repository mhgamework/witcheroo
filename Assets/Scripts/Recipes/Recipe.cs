using System.Collections.Generic;
using DefaultNamespace;

namespace Recipes
{
    public class Recipe
    {
        public List<RecipeIngredient> Ingredients = new List<RecipeIngredient>();
        public ItemType DeliveredType;
    }
}