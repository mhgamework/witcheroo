using UnityEngine;

namespace Recipes
{
    public class OrderPanelScript : MonoBehaviour
    {
        public Transform ordersPanel;
        public OrderItemViewScript itemPrefab;
        
        public OrderSystem orderSystem;

        public void Init(OrderSystem system)
        {
            this.orderSystem = system;
            updateViews();
            orderSystem.OnChange += updateViews;
            itemPrefab.gameObject.SetActive(false);
            
        }

        private void updateViews()
        {
            foreach (var k in GetComponentsInChildren<OrderItemViewScript>(true))
            {
                if (k == itemPrefab) continue;
                Destroy(k.gameObject);
            }

            foreach (var order in orderSystem.activeOrders)
            {
                var view = Instantiate(itemPrefab, ordersPanel);
                view.Show(order);
            }
        }

    }
}