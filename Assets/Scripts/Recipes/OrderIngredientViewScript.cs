using Items;
using UnityEngine;
using UnityEngine.UI;

namespace Recipes
{
    public class OrderIngredientViewScript : MonoBehaviour
    {
        public RawImage IngredientImage;
        public Text IngredientText;
        public Text OperationText;

        public void Show(RecipeIngredient ingredient)
        {
            IngredientText.text = ingredient.IconText;
            OperationText.text = ingredient.OperationText;
            gameObject.SetActive(true);

            var pref = ItemsDictionary.Instance.TryGetPrefab(ingredient.ItemType)?.gameObject;
            IngredientImage.gameObject.SetActive(pref != null);
            IngredientText.gameObject.SetActive(pref == null);
            if (pref == null)
            {
                IngredientImage.texture = null;
            }
            else
            {
                IngredientImage.texture = RenderToTextureScript.Instance().PrefabToTexture(pref);
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}