using UnityEngine;

namespace Recipes
{
    public class Order
    {
        public Recipe Recipe;
        public float OrderTime;
        public float ExpirationTime;
        public int Points = 40;
        public float PercentageExpired =>Mathf.Clamp01(1-( Time.timeSinceLevelLoad-OrderTime) / (ExpirationTime - OrderTime));
    }
}