﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace DefaultNamespace
{
    [System.Serializable]
    public class InputTranslator
    {
        public KeyCode up;
        public KeyCode upAlt;
        public KeyCode down;
        public KeyCode left;
        public KeyCode leftAlt;
        public KeyCode right;
        public KeyCode dash;
        public KeyCode channeling;
        public KeyCode placeTake;
        public KeyCode switchPlayer;
        private string Joystick = null;

        public InputTranslator(KeyCode up, KeyCode down, KeyCode left, KeyCode right, KeyCode dash, KeyCode channeling,
            KeyCode placeTake, KeyCode switchPlayer, KeyCode upAlt = KeyCode.None, KeyCode leftAlt = KeyCode.None)
        {
            this.up = up;
            this.upAlt = upAlt;
            this.down = down;
            this.left = left;
            this.leftAlt = leftAlt;
            this.right = right;
            this.dash = dash;
            this.channeling = channeling;
            this.placeTake = placeTake;
            this.switchPlayer = switchPlayer;
        }

        public InputTranslator(Dictionary<string, string> config)
        {
            if (config["up"].Contains("Joystick"))
            {
                Joystick = config["up"];
                Debug.Log(Joystick);
            }
            else
            {
                this.up = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["up"]);
                this.upAlt = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["upAlt"]);
                this.down = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["down"]);
                this.left = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["left"]);
                this.leftAlt = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["leftAlt"]);
                this.right = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["right"]);
            }

            this.dash = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["dash"]);
            this.channeling = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["channeling"]);
            this.placeTake = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["placeTake"]);
            this.switchPlayer = (KeyCode) System.Enum.Parse(typeof(KeyCode), config["switchPlayer"]);
        }

        public float GetHorizontal()
        {
            if (Joystick != null)
            {
                return Input.GetAxis($"{Joystick}Horizontal");
            }

            var speed = 0;
            if (Input.GetKey(left) || Input.GetKey(leftAlt))
            {
                speed--;
            }

            ;
            if (Input.GetKey(right))
            {
                speed++;
            }

            return speed;
        }

        public float GetVertical()
        {
            if (Joystick != null)
            {
                return Input.GetAxis($"{Joystick}Vertical");
            }

            var speed = 0;
            if (Input.GetKey(up) || Input.GetKey(upAlt))
            {
                speed++;
            }

            ;
            if (Input.GetKey(down))
            {
                speed--;
            }

            return speed;
        }

        public bool GetDash()
        {
            /*
             RF=5

            Y=3
        X=2      B=1
            A=0
             
             */
            return Input.GetKey(dash);
        }
        public bool GetDashDown()
        {
            /*
             RF=5

            Y=3
        X=2      B=1
            A=0
             
             */
            return Input.GetKeyDown(dash);
        }

        public bool GetChannel()
        {
            return Input.GetKey(channeling);
        }

        public bool GetPlaceTake()
        {
            return Input.GetKeyUp(placeTake);
        }
    }
}