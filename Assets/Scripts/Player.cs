﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public InputTranslator keySet;

    // private Rigidbody rigidbody;
    private CharacterController characterController;

    public float speed;

    public float DashCooldown = 2;
    public float DashDuration = 0.25f;
    public float LastDash = float.MinValue;
    
    public float dashModifier;

    public Transform ItemAttachPoint;
    public Item HeldItem;
    public Image m_image;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        // rigidbody = GetComponent<Rigidbody>();
        if (m_image != null) m_image.enabled = false;
    }

    public void SimulateMovementInput()
    {
        if (characterController == null) return; // Not initialized
        float moveHorizontal = keySet.GetHorizontal();
        float moveVertical = keySet.GetVertical();

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);


        var rotationDir = characterController.velocity;
            
        if (rotationDir != Vector3.zero)
        {
            var newRotation = Quaternion.RotateTowards(transform.rotation,
                                                       Quaternion.LookRotation(
                                                           new Vector3(movement.x, 0, movement.z)),
                                                       Time.deltaTime * 1000f);
            transform.localRotation = newRotation;
        }

        if (keySet.GetDash())
        {
            if (LastDash + DashCooldown < Time.timeSinceLevelLoad)
            {
                LastDash = Time.timeSinceLevelLoad;
                // Play dash effect
            }
        }

        if (LastDash + DashDuration > Time.timeSinceLevelLoad)
        {
            movement *= dashModifier;
        }

        characterController.SimpleMove(movement * speed);
        // rigidbody.AddForce(-rigidbody.velocity +movement*speed,  ForceMode.VelocityChange);
    }

    public void AttachItem(Item item)
    {
        HeldItem = item;
        item.transform.SetParent(ItemAttachPoint, false);
    }

    public void Channel(float progress, Transform itemTransform)
    {
        if (progress >= 0)
        {
            m_image.enabled = true;
        }
        else
        {
            m_image.enabled = false;
        }
        m_image.fillAmount = progress;
        if (itemTransform == null) return;

        Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, itemTransform.position);
        screenPoint.x += 40;
        screenPoint.y += 80;
        m_image.rectTransform.position = screenPoint - m_image.rectTransform.sizeDelta / 2f;
    }
}