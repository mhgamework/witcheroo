using System;
using System.Collections.Generic;
using Assets;
using Assets.Modules.MHGameWork.Reusable.UnityAdditions;
using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class NetworkedObjectRegistry : ManualSingleton<NetworkedObjectRegistry>
    {
        private List<Transform> networkPrefabs = new List<Transform>();

        public void Start()
        {
            RegisterType(PrefabsDatabase.Instance.Player.transform);
            RegisterType(PrefabsDatabase.Instance.BucketEmpty.transform);
            RegisterType(PrefabsDatabase.Instance.SpiderEye.transform);
            RegisterType(PrefabsDatabase.Instance.SpiderEyeEnchanted.transform);
        }

        public void RegisterType(Transform prefab)
        {
            networkPrefabs.Add(prefab);
        }
        public T Create<T>(T prefab,int id) where T:MonoBehaviour
        {
            var index = networkPrefabs.IndexOf(prefab.transform);
            if (index < 0) Debug.LogError("Trying to create unregistered prefab");
            var ret = Instantiate(prefab);
            var net = ret.GetOrCreateComponent<NetworkedObjectScript>();
            net.NetworkedObjectType = index;
            net.NetworkId = id;
            return ret;
        }
        public NetworkedObjectScript Create(int type, int id)
        {
            if (type >= networkPrefabs.Count) throw new Exception($"Unknown prefab type {type}");
            var pref = networkPrefabs[type];
            var ret = Instantiate(pref);
            var net = ret.GetOrCreateComponent<NetworkedObjectScript>();
            net.NetworkedObjectType = type;
            net.NetworkId = id;
            return net;
        }
    }
}