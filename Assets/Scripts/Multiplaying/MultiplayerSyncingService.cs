using System.Collections.Generic;
using System.Linq;
using MHGameWork.TheWizards;
using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class MultiplayerRoom
    {
        private NetworkedObjectRegistry registry;
        public Transform RoomTransform { get; }
        public int OwnerId { get; }
        private int nextId=0;

        public MultiplayerRoom(Transform roomTransform, int ownerId, NetworkedObjectRegistry registry)
        {
            this.RoomTransform = roomTransform;
            this.OwnerId = ownerId;
            this.registry = registry;
        }

        public void ApplyDeltas(List<EntityDelta> entityDeltas)
        {
            foreach (var d in entityDeltas)
            {
                var target = tryFindObject(d.EntityId);
                if (d.IsDelete)
                {
                    if (target == null) continue;
                    Object.Destroy(target.gameObject);
                    continue;
                }
                if (target == null)
                {
                    target = registry.Create(d.EntityType, d.EntityId);
                    target.transform.SetParent(RoomTransform);
                    target.Owner = d.Owner;
                }
                else
                {
                    if (target.Owner == OwnerId) continue; // Dont apply to owned, if already exists
                }


                target.transform.position = new Vector3(d.PositionX, d.PositionY, d.PositionZ)+RoomTransform.position;
                target.transform.rotation = Quaternion.Euler(0, d.Rotation, 0);
            }

            // Set parents
            foreach (var d in entityDeltas)
            {
                var parent = RoomTransform;
                // if (d.ParentEntityId >= 0)
                // {
                //     var ent = tryFindObject(d.ParentEntityId)?.transform;
                //     if (ent == null)
                //     {
                //         Debug.LogError($"Somehow parent is not found! {d.EntityId} {d.ParentEntityId}");
                //         ent = roomTransform;
                //     }
                // }

                tryFindObject(d.EntityId).transform.SetParent(parent, false);
            }
        }

        private NetworkedObjectScript tryFindObject(int id)
        {
            return RoomTransform.GetComponentsInChildren<NetworkedObjectScript>()
                .FirstOrDefault(k => k.NetworkId == id);
        }

        public EntityDelta createDelta(NetworkedObjectScript n)
        {
            return new EntityDelta()
            {
                EntityId = n.NetworkId,
                EntityType = n.NetworkedObjectType,
                Owner = n.Owner,
                PositionX = n.transform.position.x - RoomTransform.position.x,
                PositionY = n.transform.position.y - RoomTransform.position.y,
                PositionZ = n.transform.position.z- RoomTransform.position.z,
                Rotation = n.transform.rotation.eulerAngles.y,
            };
        }
        public EntityDelta createRemovedDelta(NetworkedObjectScript n)
        {
            return new EntityDelta()
            {
                EntityId = n.NetworkId,
                IsDelete = true,
            };
        }

        public T Create<T>(T prefab) where T : MonoBehaviour
        {
            var ret = registry.Create(prefab,nextId++);
            return ret;
        }

        public List<NetworkedObjectScript> GetAllOwnedObjects()
        {
            return GetAll()
                .Where(n => n.Owner == OwnerId).ToList();
        }
        public List<T> GetAllOwnedObjects<T>() where T:MonoBehaviour
        {
            var ff = GetAll<T>().Where(n => n.GetComponent<NetworkedObjectScript>() != null);
            var t = ff.ToList();
            return ff
                .Where(n => n.GetComponent<NetworkedObjectScript>().Owner == OwnerId).ToList();
        }

        public NetworkedObjectScript[] GetAll()
        {
            return RoomTransform.GetComponentsInChildren<NetworkedObjectScript>();
        }

        public T[] GetAll<T>()
        {
            return RoomTransform.GetComponentsInChildren<T>();
        }

        public void AssignUnassignedNetworkIds()
        {
            foreach (var n in GetAll())
            {
                if (n.NetworkId < 0) n.NetworkId = nextId++;
                n.Room = this;
            }
        }
    }
}