using System;
using System.Collections.Generic;

namespace DefaultNamespace.Multiplaying
{
    [Serializable]
    public class ClientPacket
    {
        public bool IsConnectHandshake;
        public int ClientId;
        public List<EntityDelta> Deltas = new List<EntityDelta>();
    }
}