using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class NetworkedObjectScript : MonoBehaviour
    {
        public int NetworkId=-1;
        public int NetworkedObjectType;

        public SentStates SentState;
        public int Owner;
        public MultiplayerRoom Room;

        /// <summary>
        /// Is this object owned by the room it is.
        /// This means that the room is authoriative for the object
        /// </summary>
        // public bool IsOwnedByRoom => Room.OwnerId == Owner;

        public enum SentStates
        {
            None,
            Created
        }
    }
}