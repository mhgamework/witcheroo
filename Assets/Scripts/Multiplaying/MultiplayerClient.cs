using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class MultiplayerClient
    {
        private UdpClient client;
        private ConcurrentQueue<ServerPacket> newPackets = new ConcurrentQueue<ServerPacket>();

        public void Connect()
        {
            client = new UdpClient(MultiplayerConfig.ClientPort);

            Task.Run(() => receiveLoop(CancellationToken.None));

            var p = new ClientPacket(){IsConnectHandshake = true};
            var serialize = SerializerHelper.Serialize(p);
            client.Send(serialize, serialize.Length, MultiplayerConfig.ServerAddress, MultiplayerConfig.ServerPort);
        }


        private async Task receiveLoop(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    var packet = await client.ReceiveAsync();
                    newPackets.Enqueue(SerializerHelper.Deserialize<ServerPacket>(packet.Buffer));
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

        public void SendClientPacket(ClientPacket packet)
        {
            var dgram = SerializerHelper.Serialize(packet);
            client.Send(dgram, dgram.Length, MultiplayerConfig.ServerAddress, MultiplayerConfig.ServerPort);
        }

        public List<ServerPacket> GetNewServerPackets()
        {
            var l = new List<ServerPacket>();
            while (newPackets.TryDequeue(out var p)) l.Add(p);
            return l;
        }
    }
}