using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class MultiplayerConfig
    {
        public static bool IsServerFolder => !Application.dataPath.Contains("Client");
        public static int ServerPort = 15341;
        public static int ClientPort;
        public static string ServerAddress = "127.0.0.1";//"192.168.0.178";
        public static string ClientAddress = "127.0.0.1";
    }
}