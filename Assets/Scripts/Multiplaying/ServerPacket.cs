using System;
using System.Collections.Generic;

namespace DefaultNamespace.Multiplaying
{
    [Serializable]
    public class ServerPacket
    {
        public bool IsHandshake;
        public int ClientId;

        public List<EntityDelta> Deltas = new List<EntityDelta>();
    }
}