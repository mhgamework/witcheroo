using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class MultiplayerServer
    {
        private UdpClient server;
        private List<IPEndPoint> clients = new List<IPEndPoint>();
        private ConcurrentQueue<ClientPacket> newPackets = new ConcurrentQueue<ClientPacket>();

        private CancellationTokenSource tokenSource =new CancellationTokenSource();
        
        public void StartListen()
        {
            var list = new List<int>();

            server = new UdpClient(MultiplayerConfig.ServerPort);
            Task.Run(() => receiveLoop(tokenSource.Token));
        }

        private async Task receiveLoop(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    var packet = await server.ReceiveAsync();
                    if (!clients.Contains(packet.RemoteEndPoint)) onNewClient(packet.RemoteEndPoint);
                    newPackets.Enqueue(SerializerHelper.Deserialize<ClientPacket>(packet.Buffer));
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }

        private void onNewClient(IPEndPoint packetRemoteEndPoint)
        {
            clients.Add(packetRemoteEndPoint);
        }

        public void SendDeltaPacket(ServerPacket serverPacket)
        {
            var datagram = SerializerHelper.Serialize(serverPacket);

            foreach (var c in clients)
            {
                server.SendAsync(datagram, datagram.Length, c);
            }
        }

        public List<ClientPacket> GetNewClientPackets()
        {
            var ret = new List<ClientPacket>();
            while (newPackets.TryDequeue(out var p))
            {
                ret.Add(p);
            }

            return ret;
        }

        public void StopListen()
        {
            server.Close();
            tokenSource.Cancel();
        }
    }
}