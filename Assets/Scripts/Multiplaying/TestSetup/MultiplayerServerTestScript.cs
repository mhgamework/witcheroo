using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace.Bootstrap;
using DefaultNamespace.Menus;
using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class MultiplayerServerTestScript : MonoBehaviour
    {

        private MultiplayerRoom room;
        private MultiplayerServer multiplayerServer;

        private int numClients = 0;
        private bool started = false;

        public bool enabledBasedOnProjectDirName;

        private CompositionRoot compositionRoot;

        public void Start()
        {
            if (enabledBasedOnProjectDirName && !MultiplayerConfig.IsServerFolder) gameObject.SetActive(false);
        }

        private void OnDisable()
        {
            multiplayerServer.StopListen();
        }

        private void onClientConnected(int id)
        {
            // var p =room.Create(PrefabsDatabase.Instance.Player);
            // p.transform.position = new Vector3(0, 5, 0);
            // p.transform.SetParent(SceneRoot, false);
            // p.GetComponent<NetworkedObjectScript>().Owner = id;
        }

        private HashSet<NetworkedObjectScript> sentObjects = new HashSet<NetworkedObjectScript>();

        public void InitAndStartServer(CompositionRoot compositionRoot, bool ownAllPlayers)
        {
            multiplayerServer = new MultiplayerServer();
            room = compositionRoot.Room;
            this.compositionRoot = compositionRoot;

            multiplayerServer.StartListen();

            onClientConnected(numClients++);

            // Assign ids to objects already in scene
            room.AssignUnassignedNetworkIds();

            if (ownAllPlayers)
            {
                var ps = room.GetAll<Player>();
                for (var i = 0; i < ps.Length; i++)
                {
                    var p = ps[i];
                    p.keySet = new InputTranslator(compositionRoot.ConfigLoader.Sections[$"InputMapping{i + 1}"]);
                    p.GetComponent<NetworkedObjectScript>().Owner = 0;
                }
            }
            else
            {
                var ps = room.GetAll<Player>();
                ps[0].keySet = new InputTranslator(compositionRoot.ConfigLoader.Sections["InputMapping1"]);
                for (int i = 0; i < ps.Length; i++)
                {
                    ps[i].GetComponent<NetworkedObjectScript>().Owner = i;
                }
            }
            
            
            compositionRoot.GameSimulator.StartLevel(compositionRoot.LevelStats.Description);
            started = true;
        }

        public void Update()
        {
            if (!started) return;

            Services.WithContext(compositionRoot, () =>
            {
                compositionRoot.GameSimulator.ApplyPlayerInputs();
                compositionRoot.GameSimulator.SimulateGameLogic();

                receiveAndSendNetworkDeltas();
            });
        }

        private void receiveAndSendNetworkDeltas()
        {
            foreach (var c in multiplayerServer.GetNewClientPackets())
            {
                if (c.IsConnectHandshake)
                {
                    onClientConnected(numClients++);
                    continue;
                }

                room.ApplyDeltas(c.Deltas);
            }

            var packet = new ServerPacket();

            foreach (var n in room.GetAll())
            {
                sentObjects.Add(n);
                packet.Deltas.Add(room.createDelta(n));
                n.SentState = NetworkedObjectScript.SentStates.Created;
            }

            foreach (var deleted in sentObjects.Where(f => f == null).ToList())
            {
                sentObjects.Remove(deleted);
                packet.Deltas.Add(room.createRemovedDelta(deleted));
            }

            multiplayerServer.SendDeltaPacket(packet);
        }


        public void FixedUpdate()
        {
            if (!started) return;
            compositionRoot?.GameSimulator?.SimulatePlayerMovementFixedTimestep();
        }

    }
}