using System;
using System.Linq;
using DefaultNamespace.Bootstrap;
using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class MultiplayerClientTestScript : MonoBehaviour
    {
        private MultiplayerRoom room;

        private MultiplayerClient multiplayerClient;
        private bool started;

        public bool enabledBasedOnProjectDirName;
        private CompositionRoot compositionRoot;

        public void Start()
        {
            if (enabledBasedOnProjectDirName && MultiplayerConfig.IsServerFolder)
                gameObject.SetActive(false);
        }

        public void InitAndConnect(CompositionRoot root)
        {
            multiplayerClient = new MultiplayerClient();
            room = root.Room;
            compositionRoot = root;
            multiplayerClient.Connect();
            started = true;
        }

        public void Update()
        {
            if (!started) return;

            foreach (var p in room.GetAll<Player>())
            {
                p.keySet = new InputTranslator(compositionRoot.ConfigLoader.Sections["InputMapping2"]);
            }

            compositionRoot.GameSimulator.ApplyPlayerInputs();

            applyAndSendDeltas();
        }

        private void applyAndSendDeltas()
        {
            foreach (var p in multiplayerClient.GetNewServerPackets())
                room.ApplyDeltas(p.Deltas);

            var clientPacket = new ClientPacket();
            clientPacket.ClientId = 1;
            clientPacket.Deltas = room.GetAllOwnedObjects().Select(k => room.createDelta(k)).ToList();
            multiplayerClient.SendClientPacket(clientPacket);
        }

        public void FixedUpdate()
        {
            if (!started) return;
            compositionRoot?.GameSimulator?.SimulatePlayerMovementFixedTimestep();
        }
    }
}