using Assets.Modules.MHGameWork.Reusable.UnityAdditions;
using UnityEngine;

namespace DefaultNamespace.Multiplaying
{
    public class PrefabsDatabase : ManualSingleton<PrefabsDatabase>
    {
        public Player Player;
        public Item BucketEmpty;
        public Item SpiderEye;
        public Item SpiderEyeEnchanted;
    }
}