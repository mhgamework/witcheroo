using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DefaultNamespace.Multiplaying
{
    public class SerializerHelper
    {
        public static byte[] Serialize(object obj)
        {
            var f = new BinaryFormatter();
            var mem = new MemoryStream();
            f.Serialize(mem, obj);
            return mem.ToArray();
        }

        public static T Deserialize<T>(byte[] data)
        {
            var mem = new MemoryStream(data);
            var f = new BinaryFormatter();
            return (T) f.Deserialize(mem);
        }
    }
}