using System;

namespace DefaultNamespace.Multiplaying
{
    [Serializable]
    public struct EntityDelta
    {
        public int EntityId;
        public int EntityType;
        public float Rotation;
        public float PositionX;
        public float PositionY;
        public float PositionZ;
        public int ParentEntityId;
        public int Owner;
        public bool IsDelete;
    }
}