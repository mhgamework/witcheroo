﻿using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;


namespace DefaultNamespace
{
    public class ConfigLoader
    {
        private Dictionary<string, Dictionary<string, string>> sections =
            new Dictionary<string, Dictionary<string, string>>();

        public ConfigLoader(string path = null)
        {
            if (path == null)
            {
                path = Path.Combine(Application.streamingAssetsPath, "config.txt");
            }

            string[] lines = File.ReadAllLines(path);
            var section_name = "";
            foreach (string line in lines)
            {
                if (line.StartsWith("#"))
                {
                    continue;
                }
                else if (line.StartsWith("["))
                {
                    section_name = line.Substring(1, line.Length - 2);
                    sections.Add(section_name, new Dictionary<string, string>());
                }
                else
                {
                    string[] keyVal = line.Split('=');
                    sections[section_name].Add(keyVal[0], keyVal[1]);
                }
            }
        }

        public Dictionary<string, Dictionary<string, string>> Sections
        {
            get => sections;
            set => sections = value;
        }
    }
}