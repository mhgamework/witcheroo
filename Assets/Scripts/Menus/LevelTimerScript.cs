using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Menus
{
    public class LevelTimerScript : MonoBehaviour
    {
        public Text Text;
        private LevelStats levelStats;

        public void Init(LevelStats levelStats)
        {
            this.levelStats = levelStats;
        }

        public void Update()
        {
            if (levelStats == null) return;
            var timeInLevel = Time.timeSinceLevelLoad - levelStats.StartTime;
            var timeLeft = levelStats.Description.LevelDurationSeconds - timeInLevel;
            timeLeft = Mathf.Max(0, timeLeft);

            var mins = Mathf.CeilToInt(timeLeft) / 60;
            var secs = Mathf.CeilToInt(timeLeft) % 60;

            var partA = mins > 0 ? $"{mins}:" : "";
            Text.text = $"{partA}{secs:00}";
        }
    }
}