using System;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Menus
{
    public class ScoreDialogScript : MonoBehaviour
    {
        public Button btnRestart;
        public Button btnLevelSelect;
        public Text txtScore1;
        public Text txtScore2;
        public Text txtScore3;
        public Image imgScore1;
        public Image imgScore2;
        public Image imgScore3;
        public Text txtPotionsDelivered;
        public Text txtPotionsFailed;
        public Text txtLevelName;
        public Text txtScore;
            


        public void Init()
        {
            Hide();
        }
        
        public void Show(LevelStats stats, Action onRestart, Action onLevelSelect)
        {
            var desc = stats.Description;
            btnRestart.onClick.RemoveAllListeners();
            btnRestart.onClick.AddListener(() => onRestart());
            
            btnLevelSelect.onClick.RemoveAllListeners();
            btnLevelSelect.onClick.AddListener(() => onLevelSelect());

            txtScore1.text = desc.ScoreStars1.ToString();
            txtScore2.text = desc.ScoreStars2.ToString();
            txtScore3.text = desc.ScoreStars3.ToString();

            imgScore1.color = stats.Score >= desc.ScoreStars1 ? Color.white : Color.black;
            imgScore2.color = stats.Score >= desc.ScoreStars2 ? Color.white : Color.black;
            imgScore3.color = stats.Score >= desc.ScoreStars3 ? Color.white : Color.black;

            txtPotionsDelivered.text = $"Potions delivered: {stats.NumDeliveredPotions}";
            txtPotionsFailed.text = $"Potions failed: {stats.NumFailedPotions}";

            txtLevelName.text = desc.LevelName;

            txtScore.text = stats.Score.ToString();
            
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

    
    }
}