namespace DefaultNamespace.Menus
{
    public class LevelStats
    {
        public LevelDescription Description;
        public float StartTime;
        public int Score;
        public int NumDeliveredPotions;
        public int NumFailedPotions;
        public bool IsLevelPlaying;

        public LevelStats()
        {
        }
    }
}