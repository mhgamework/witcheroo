using DefaultNamespace.Bootstrap;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace.Menus
{
    [ExecuteInEditMode]
    public class LevelButtonScript : MonoBehaviour
    {
        public string SceneName;
        public string Text;

        public void Start()
        {
            GetComponent<Button>().onClick.AddListener( onClick);
        }

        public void Update()
        {
            GetComponentInChildren<Text>().text = Text;

        }

        private void onClick()
        {
            if (!Application.isPlaying) return;
            GameStateService.SwitchToLevel(SceneName);
        }
    }
}