using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace DefaultNamespace.Menus
{
    [Serializable]
    public class LevelDescription
    {
        public string LevelName;
        public int ScoreStars1;
        public int ScoreStars2;
        public int ScoreStars3;
        public int LevelDurationSeconds;
        public float FirstRecipeTime = 3;
        public List<LevelRecipeSpec> Recipes = new List<LevelRecipeSpec>();

    }

    [Serializable]
    public class LevelRecipeSpec
    {
        public ItemType ResultPotion;
        /// <summary>
        /// Relative weight
        /// </summary>
        public float ChanceForThisPotion;
        public float OrderExpirationTime = 70;
        /// <summary>
        /// Potiontime + [-PotionTimeDeviation,PotionTimeDeviation]
        /// </summary>
        public float OrderExpirationTimeDeviation=30;

        /// <summary>
        /// Time before next recipe arrives
        /// </summary>
        public float NextRecipeInterval = 17.5f;
        public float NextRecipeIntervalDeviation = 2.5f;

        public float GetRandomOrderExpirationTime() => 
            OrderExpirationTime + Random.Range(-1f, 1f) * OrderExpirationTimeDeviation;
        public float GetRandomNextRecipeInterval() => 
            NextRecipeInterval + Random.Range(-1f, 1f) * NextRecipeIntervalDeviation;
    }
}